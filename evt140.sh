#!/bin/bash
cagetoutput=$(caget Utg-Ymir:TS-EVR-01:EvtHCnt-I)
mystrcnt=`echo $cagetoutput | awk '{print $NF}'`
startval=`expr $mystrcnt`
echo "previous counter value is $startval"
caput Utg-VIP:ics-EVG-01:TrigEvt1-EvtCode-SP 140 > /dev/null 2>&1
caput Utg-VIP:ics-EVG-01:TrigEvt1-TrigSrc-Sel "Mxc7" > /dev/null 2>&1
let "nextval = $startval+1"
# echo "nexval is $nextval"
while [ $startval -ne $nextval ]
do
    sleep 0.1
    cagetoutput=$(caget Utg-Ymir:TS-EVR-01:EvtHCnt-I)
    mystrcnt=`echo $cagetoutput | awk '{print $NF}'`
    startval=`expr $mystrcnt`
done
echo "new counter value is $startval"
caget Utg-Ymir:TS-EVR-01:EvtHCnt-I > /dev/null 2>&1
caput Utg-VIP:ics-EVG-01:TrigEvt1-TrigSrc-Sel "Off" > /dev/null 2>&1
